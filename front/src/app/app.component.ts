import { Component, OnInit } from '@angular/core';
import { UserService } from './service/user.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string = 'Front-end';
  users: User;

  constructor(
    private userService: UserService,
  ) {
    //
  }

  ngOnInit() {
    //
    this.userService.getUsers().subscribe(data => {
      console.log(data);
    });
  }
}
