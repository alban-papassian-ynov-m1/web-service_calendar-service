import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { GenericService } from './generic.service';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService extends GenericService {

  serviceUrl: string = 'users';

  constructor(
    private http: HttpClient,
    public router: Router,
  ) {
    super();
  }

  getUser(id): Observable<User> {
    return this.http.get<User>(this.apiURL + '/' + this.serviceUrl + '/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getUsers(): Observable<User> {
    return this.http.get<User>(this.apiURL + '/' + this.serviceUrl + '/')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  deleteUser(id) {
    return this.http.delete<User>(this.apiURL + '/' + this.serviceUrl + '/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  updateUser(id, user): Observable<User> {
    return this.http.put<User>(this.apiURL + '/' + this.serviceUrl + '/' + id, JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  createUser(user): Observable<User> {
    return this.http.post<User>(this.apiURL + '/' + this.serviceUrl + '/', JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
}
