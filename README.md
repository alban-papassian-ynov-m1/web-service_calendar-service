# Web service : calendar service

## Introduction
NestJS, TypeOrm, Swagger

## Organisation du projet

Pour chaque donnée du projet, il est mis en oeuvre 

### Controller
Les controllers représentent les points d'entrés de l'API. 
Chaque controller possède 5 méthode et donc 5 points d'entrés : GetOne, GetAll, Create, Update, Delete.
Chacun des méthodes permet de faire appel aux méthodes correspondandant du service associé.

A savoir :
- GetOne: Récupère un objet en fonction de son id
- GetAll : Récupère tous les objets
- Create : Créer un objet
- Update : Met à jour un objet
- Delete : Supprime un objet en fonction de son id

### Services
Les services permettent d'écrire le traitement métier appliqué à chacunes des méthodes.
C'est dans ces services que la communication avec la base de donnée est effectué.

### Dto
Pour chaque entité est définie un DTO associé.
Cela permet d'avoir accès à des interfaces qui représente chacun des objets du projet.

### Responses
Chacune des méthodes du controller renvois une `response`, en fonction de l'entité appelé.
Toute les `responses` sont des `extends` d'un type generic qui défini les élements classiques, qui doivent être présent dans chacune des `reponses`. (success, message, error, statusCode)

### Entity
Toute les entités sont déclaré dans le dossier entities. Pour chacune de ces entités, on retrouve 2 méthodes : 'fromDto' et 'toDto'.
Ces méthodes permettent de 'mapper' l'entité (coté bdd) vers un DTO (côté objet), et vise versa.

## Contrat d'interface

L'objectif de ce document est de recenser l'ensemble des routes disponibles dans l'API .
On y retrouve l'URL via laquelle on peux utiliser l'API, la méthode associé, les paramètres à fournir lors de l'appel ainsi que le type de retour.

| route | method | params | response |
|--|--|--|--|
| users | get |  | UserDto[] |
| users/:userId | get | userId: string | UserDto |
| users | post | UserDto | UserDto |
| users | put | UserDto | UserDto |
| users/:userId | delete | userId: string | GenericReponse |
|  |  |  | |
| agendas | get |  | AgendaDto[] |
| agendas/:agendaId | get | agendaId: string | AgendaDto |
| agendas | post | AgendaDto | AgendaDto |
| agendas | put | AgendaDto | AgendaDto |
| agendas/:agendaId | delete | agendaId: string | GenericReponse |
|  |  |  | |
| meetings | get |  | MeetingDto[] |
| meetings/:meetingId | get | meetingId: string | MeetingDto|
| meetings | post | MeetingDto| MeetingDto|
| meetings | put | MeetingDto| MeetingDto|
| meetings/:meetingId | delete | meetingId: string | GenericResponse |

## Sécurité
Pour chaque controller, il est possible de définir un `guard` afin de gérer l'accès aux méthodes. On peux basé ce système de guard en fonction de n'importe quel critère souhaité (exemple: l'utilisateur possède le role `admin`).

## Gérer les ressources
 
### Entité
| colonne| type|
|--|--|
| id| varchar|
| username| varchar|


## Gérer les agendas

 ### Entité
| colonne | type |
|--|--|
| id| varchar|
| label| varchar|
| user| user|


## Gestion des rendez-vous

 ### Entité
| colonne| type|
|--|--|
| id| varchar|
| label| varchar|
| dateBegin| datetime|
| dateEnd| datetime|
| agenda| agenda|
