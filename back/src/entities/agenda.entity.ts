import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { AgendaDto } from 'src/models/dto/agenda.dto';
import { User } from './user.entity';
import { Meeting } from './meeting.entity';

@Entity({ name: 'agendas' })
export class Agenda {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'label', nullable: false })
    label: string;

    @ManyToOne(() => User, user => user.agendas)
    user: User;

    @OneToMany(() => Meeting, meeting => meeting.agenda, { cascade: true })
    meetings: Meeting[];

    @Column('varchar', { name: 'userId', nullable: false })
    userId: string;

    public toDto(): AgendaDto {
        return {
            id: this.id,
            label: this.label,

            userId: this.userId,

            user: this.user ? this.user.toDto() : undefined,
            meetings: this.meetings ? this.meetings.map(x => x.toDto()) : [],
        };
    }

    public fromDto(dto: AgendaDto) {
        this.id = dto.id;
        this.label = dto.label;

        this.userId = dto.userId;

        if (dto.user) {
            const user = new User();
            user.fromDto(dto.user);
            this.user = user;
            this.userId = dto.userId;
        }

        if (dto.meetings) {
            this.meetings = [];
            dto.meetings.forEach(meetingDto => {
                const meeting = new Meeting();
                meeting.fromDto(meetingDto);
                this.meetings.push(meeting);
            });
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
