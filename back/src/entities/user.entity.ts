import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { UserDto } from 'src/models/dto/user.dto';
import { Agenda } from './agenda.entity';

@Entity({ name: 'users' })
export class User {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'username', nullable: false })
    username: string;

    @OneToMany(() => Agenda, agenda => agenda.user, { cascade: true })
    agendas: Agenda[];

    public toDto(): UserDto {
        return {
            id: this.id,
            username: this.username,

            agendas: this.agendas ? this.agendas.map(x => x.toDto()) : [],
        };
    }

    public fromDto(dto: UserDto) {
        this.id = dto.id;
        this.username = dto.username;

        if (dto.agendas) {
            this.agendas = [];
            dto.agendas.forEach(agendaDto => {
                const agenda = new Agenda();
                agenda.fromDto(agendaDto);
                this.agendas.push(agenda);
            });
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
