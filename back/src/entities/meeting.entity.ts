import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Agenda } from './agenda.entity';
import { MeetingDto } from 'src/models/dto/meeting.dto.';

@Entity({ name: 'meetings' })
export class Meeting {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'label', nullable: false })
    label: string;

    @Column('datetime', { name: 'dateBegin', nullable: false })
    dateBegin: Date;

    @Column('datetime', { name: 'dateEnd', nullable: false })
    dateEnd: Date;

    @Column('varchar', { name: 'agendaId', nullable: true })
    agendaId?: string;

    @ManyToOne(() => Agenda, agenda => agenda.meetings)
    @JoinColumn({ name: 'agendaId' })
    agenda?: Agenda;

    public toDto(): MeetingDto {
        return {
            id: this.id,
            label: this.label,
            dataBegin: this.dateBegin,
            dateEnd: this.dateEnd,

            agendaId: this.agendaId,

            agenda: this.agenda ? this.agenda.toDto() : undefined,
        };
    }

    public fromDto(dto: MeetingDto) {
        this.id = dto.id;
        this.label = dto.label;
        this.dateBegin = dto.dataBegin;
        this.dateEnd = dto.dateEnd;

        this.agendaId = dto.agendaId;

        if (dto.agenda) {
            const agenda = new Agenda();
            agenda.fromDto(dto.agenda);
            this.agenda = agenda;
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
