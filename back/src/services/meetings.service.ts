import { Injectable } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { Meeting } from 'src/entities/meeting.entity';
import { GetMeetingsResponse, GetMeetingResponse } from 'src/models/responses/meeting.responses';
import { GenericResponse } from 'src/models/responses/generic.responses';
import { MeetingDto } from 'src/models/dto/meeting.dto.';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MeetingsService {
    constructor(
        @InjectRepository(Meeting)
        private readonly meetingRepository: Repository<Meeting>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<Meeting>): Promise<GetMeetingsResponse> {
        const response = new GetMeetingsResponse();
        try {
            conditions.relations = [];
            const meetings = await this.meetingRepository.find(conditions);

            if (meetings) {
                response.meetings = meetings.map(x => x.toDto());
                response.totalMeetingsCount = await this.meetingRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions?: FindOneOptions<Meeting>): Promise<GetMeetingResponse> {
        const response = new GetMeetingResponse();
        try {
            conditions.relations = [];
            const meeting = await this.meetingRepository.findOne(conditions);

            if (meeting) {
                response.meeting = meeting.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async create(meeting: MeetingDto): Promise<GetMeetingResponse> {
        const response = new GetMeetingResponse();
        try {
            let meetingEntity = new Meeting();

            meetingEntity.fromDto(meeting);

            meetingEntity = await this.meetingRepository.save(meeting);
            meetingEntity = await this.meetingRepository.findOne({ id: meetingEntity.id });

            response.meeting = meetingEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async update(meeting: MeetingDto): Promise<GetMeetingResponse> {
        const response = new GetMeetingResponse();
        try {
            let meetingEntity = await this.meetingRepository.findOne({ id: meeting.id }, { relations: [] });
            if (!meetingEntity) {
                throw new Error('No meeting with id => ' + meeting.id);
            }

            meetingEntity.fromDto(meeting);

            meetingEntity = await this.meetingRepository.save(meeting);
            meetingEntity = await this.meetingRepository.findOne({ id: meetingEntity.id });

            response.meeting = meetingEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(meetingId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const meeting = await this.meetingRepository.findOne({ id: meetingId }, { relations: [] });
            if (!meeting) {
                throw new Error('No meeting with id => ' + meetingId);
            }
            await this.meetingRepository.delete(meetingId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
