import { Injectable } from '@nestjs/common';
import { GetUsersResponse, GetUserResponse } from 'src/models/responses/user.responses';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { User } from 'src/entities/user.entity';
import { UserDto } from 'src/models/dto/user.dto';
import { GenericResponse } from 'src/models/responses/generic.responses';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<User>): Promise<GetUsersResponse> {
        const response = new GetUsersResponse();
        try {
            conditions.relations = ['agendas', 'agendas.meetings'];
            const users = await this.userRepository.find(conditions);

            if (users) {
                response.users = users.map(x => x.toDto());
                response.totalUsersCount = await this.userRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions?: FindOneOptions<User>): Promise<GetUserResponse> {
        const response = new GetUserResponse();
        try {
            conditions.relations = ['agendas', 'agendas.meetings'];
            const user = await this.userRepository.findOne(conditions);

            if (user) {
                response.user = user.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async create(user: UserDto): Promise<GetUserResponse> {
        const response = new GetUserResponse();
        try {
            let userEntity = new User();

            userEntity.fromDto(user);

            userEntity = await this.userRepository.save(user);
            userEntity = await this.userRepository.findOne({ id: userEntity.id });

            response.user = userEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async update(user: UserDto): Promise<GetUserResponse> {
        const response = new GetUserResponse();
        try {
            let userEntity = await this.userRepository.findOne({ id: user.id }, { relations: [] });
            if (!userEntity) {
                throw new Error('No user with id => ' + user.id);
            }

            userEntity.fromDto(user);

            userEntity = await this.userRepository.save(user);
            userEntity = await this.userRepository.findOne({ id: userEntity.id });

            response.user = userEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(userId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const user = await this.userRepository.findOne({ id: userId }, { relations: [] });
            if (!user) {
                throw new Error('No user with id => ' + userId);
            }
            await this.userRepository.delete(userId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
