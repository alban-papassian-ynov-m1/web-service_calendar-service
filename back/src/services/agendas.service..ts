import { Injectable } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { GetAgendasResponse, GetAgendaResponse } from 'src/models/responses/agenda.responses';
import { GenericResponse } from 'src/models/responses/generic.responses';
import { Agenda } from '../entities/agenda.entity';
import { AgendaDto } from '../models/dto/agenda.dto';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AgendasService {
    constructor(
        @InjectRepository(Agenda)
        private readonly agendaRepository: Repository<Agenda>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<Agenda>): Promise<GetAgendasResponse> {
        const response = new GetAgendasResponse();
        try {
            conditions.relations = [];
            const agendas = await this.agendaRepository.find(conditions);

            if (agendas) {
                response.agendas = agendas.map(x => x.toDto());
                response.totalAgendasCount = await this.agendaRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions?: FindOneOptions<Agenda>): Promise<GetAgendaResponse> {
        const response = new GetAgendaResponse();
        try {
            conditions.relations = [];
            const agenda = await this.agendaRepository.findOne(conditions);

            if (agenda) {
                response.agenda = agenda.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async create(agenda: AgendaDto): Promise<GetAgendaResponse> {
        const response = new GetAgendaResponse();
        try {
            let agendaEntity = new Agenda();

            agendaEntity.fromDto(agenda);

            agendaEntity = await this.agendaRepository.save(agenda);
            agendaEntity = await this.agendaRepository.findOne({ id: agendaEntity.id });

            response.agenda = agendaEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async update(agenda: AgendaDto): Promise<GetAgendaResponse> {
        const response = new GetAgendaResponse();
        try {
            let agendaEntity = await this.agendaRepository.findOne({ id: agenda.id }, { relations: [] });
            if (!agendaEntity) {
                throw new Error('No agenda with id => ' + agenda.id);
            }

            agendaEntity.fromDto(agenda);

            agendaEntity = await this.agendaRepository.save(agenda);
            agendaEntity = await this.agendaRepository.findOne({ id: agendaEntity.id });

            response.agenda = agendaEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(agendaId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const agenda = await this.agendaRepository.findOne({ id: agendaId }, { relations: [] });
            if (!agenda) {
                throw new Error('No agenda with id => ' + agendaId);
            }
            await this.agendaRepository.delete(agendaId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
