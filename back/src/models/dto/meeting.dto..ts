import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { AgendaDto } from './agenda.dto';

export class MeetingDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    label: string;

    @ApiModelProperty()
    dataBegin: Date;

    @ApiModelProperty()
    dateEnd: Date;

    @ApiModelProperty({ type: () => AgendaDto })
    agenda: AgendaDto;

    @ApiModelProperty()
    agendaId: string;
}
