import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { AgendaDto } from './agenda.dto';

export class UserDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    username: string;

    @ApiModelPropertyOptional({ type: () => AgendaDto, isArray: true })
    agendas?: AgendaDto[];
}
