import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserDto } from './user.dto';
import { MeetingDto } from './meeting.dto.';

export class AgendaDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    label: string;

    @ApiModelProperty({ type: () => UserDto })
    user: UserDto;

    @ApiModelProperty()
    userId: string;

    @ApiModelPropertyOptional({ type: () => MeetingDto, isArray: true })
    meetings?: MeetingDto[];
}
