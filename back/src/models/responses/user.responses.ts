import { GenericResponse } from './generic.responses';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserDto } from '../dto/user.dto';

export class GetUserResponse extends GenericResponse {
    @ApiModelProperty()
    user: UserDto;
    constructor() {
        super();
        this.user = null;
    }
}

export class GetUsersResponse extends GenericResponse {
    @ApiModelProperty()
    users: UserDto[];

    @ApiModelProperty()
    totalUsersCount: number;
    constructor() {
        super();
        this.users = [];
        this.totalUsersCount = 0;
    }
}
