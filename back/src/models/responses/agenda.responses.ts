import { GenericResponse } from './generic.responses';
import { ApiModelProperty } from '@nestjs/swagger';
import { AgendaDto } from '../dto/agenda.dto';

export class GetAgendaResponse extends GenericResponse {
    @ApiModelProperty()
    agenda: AgendaDto;
    constructor() {
        super();
        this.agenda = null;
    }
}

export class GetAgendasResponse extends GenericResponse {
    @ApiModelProperty()
    agendas: AgendaDto[];

    @ApiModelProperty()
    totalAgendasCount: number;
    constructor() {
        super();
        this.agendas = [];
        this.totalAgendasCount = 0;
    }
}
