import { GenericResponse } from './generic.responses';
import { ApiModelProperty } from '@nestjs/swagger';
import { MeetingDto } from '../dto/meeting.dto.';

export class GetMeetingResponse extends GenericResponse {
    @ApiModelProperty()
    meeting: MeetingDto;
    constructor() {
        super();
        this.meeting = null;
    }
}

export class GetMeetingsResponse extends GenericResponse {
    @ApiModelProperty()
    meetings: MeetingDto[];

    @ApiModelProperty()
    totalMeetingsCount: number;
    constructor() {
        super();
        this.meetings = [];
        this.totalMeetingsCount = 0;
    }
}
