import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './controllers/users.controller';
import { UsersService } from './services/users.service';
import { User } from './entities/user.entity';
import { Agenda } from './entities/agenda.entity';
import { AgendasService } from './services/agendas.service.';
import { AgendasController } from './controllers/agendas.controller';
import { MeetingsController } from './controllers/meetings.controller';
import { MeetingsService } from './services/meetings.service';
import { Meeting } from './entities/meeting.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'calendar_service',
      logging: true,
      entities: [
        __dirname + '/entities/*.entity{.ts,.js}',
      ],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      User,
      Agenda,
      Meeting,
    ]),
  ],
  controllers: [
    AppController,
    UsersController,
    AgendasController,
    MeetingsController,
  ],
  providers: [
    AppService,
    UsersService,
    AgendasService,
    MeetingsService,
  ],
})
export class AppModule { }
