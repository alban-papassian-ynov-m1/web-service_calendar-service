import { UsersService } from 'src/services/users.service';
import { Controller, HttpCode, Param, Get, Post, Body, Delete, Put, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { GetUserResponse, GetUsersResponse } from 'src/models/responses/user.responses';
import { FindOneOptions } from 'typeorm';
import { User } from 'src/entities/user.entity';
import { UserDto } from 'src/models/dto/user.dto';
import { GenericResponse } from 'src/models/responses/generic.responses';

@Controller('users')
@ApiUseTags('users')
export class UsersController {
    constructor(
        private usersService: UsersService,
    ) {
        //
    }

    @Get(':userId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get user', operationId: 'getUser' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get user', type: GetUserResponse })
    @HttpCode(HttpStatus.OK)
    async get(@Param('userId') userId: string): Promise<GetUserResponse> {
        const findOneOptions: FindOneOptions<User> = { where: { id: userId } };
        return await this.usersService.findOne(findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all users', operationId: 'getAllUsers' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get all users', type: GetUsersResponse })
    @HttpCode(HttpStatus.OK)
    async getAll(): Promise<GetUsersResponse> {
        return await this.usersService.findAll({});
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create', operationId: 'CreateUser' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Create', type: GetUserResponse })
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() user: UserDto): Promise<GetUserResponse> {
        return await this.usersService.create(user);
    }

    @Put()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update', operationId: 'UpdateUser' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Update', type: GetUserResponse })
    @HttpCode(HttpStatus.OK)
    async update(@Body() user: UserDto): Promise<GetUserResponse> {
        return await this.usersService.update(user);
    }

    @Delete(':userId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete', operationId: 'DeleteUser' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Delete', type: GenericResponse })
    @HttpCode(HttpStatus.OK)
    async delete(@Param('userId') userId: string): Promise<GenericResponse> {
        return await this.usersService.delete(userId);
    }
}
