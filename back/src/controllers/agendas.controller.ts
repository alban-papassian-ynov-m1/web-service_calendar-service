import { Controller, HttpCode, Param, Get, Post, Body, Delete, Put, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { AgendasService } from 'src/services/agendas.service.';
import { GetAgendaResponse, GetAgendasResponse } from 'src/models/responses/agenda.responses';
import { FindOneOptions } from 'typeorm';
import { Agenda } from 'src/entities/agenda.entity';
import { AgendaDto } from 'src/models/dto/agenda.dto';
import { GenericResponse } from 'src/models/responses/generic.responses';

@Controller('agendas')
@ApiUseTags('agendas')
export class AgendasController {
    constructor(
        private agendasService: AgendasService,
    ) {
        //
    }

    @Get(':agendaId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get agenda', operationId: 'getAgenda' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get agenda', type: GetAgendaResponse })
    @HttpCode(HttpStatus.OK)
    async get(@Param('agendaId') agendaId: string): Promise<GetAgendaResponse> {
        const findOneOptions: FindOneOptions<Agenda> = { where: { id: agendaId } };
        return await this.agendasService.findOne(findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all agendas', operationId: 'getAllAgendas' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get all agendas', type: GetAgendasResponse })
    @HttpCode(HttpStatus.OK)
    async getAll(): Promise<GetAgendasResponse> {
        return await this.agendasService.findAll({});
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create', operationId: 'CreateAgenda' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Create', type: GetAgendaResponse })
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() agenda: AgendaDto): Promise<GetAgendaResponse> {
        return await this.agendasService.create(agenda);
    }

    @Put()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update', operationId: 'UpdateAgenda' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Update', type: GetAgendaResponse })
    @HttpCode(HttpStatus.OK)
    async update(@Body() agenda: AgendaDto): Promise<GetAgendaResponse> {
        return await this.agendasService.update(agenda);
    }

    @Delete(':agendaId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete', operationId: 'DeleteAgenda' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Delete', type: GenericResponse })
    @HttpCode(HttpStatus.OK)
    async delete(@Param('agendaId') agendaId: string): Promise<GenericResponse> {
        return await this.agendasService.delete(agendaId);
    }
}
