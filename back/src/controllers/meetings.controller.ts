import { Controller, HttpCode, Param, Get, Post, Body, Delete, Put, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { FindOneOptions } from 'typeorm';
import { MeetingsService } from 'src/services/meetings.service';
import { GetMeetingResponse, GetMeetingsResponse } from 'src/models/responses/meeting.responses';
import { MeetingDto } from 'src/models/dto/meeting.dto.';
import { GenericResponse } from 'src/models/responses/generic.responses';
import { Meeting } from 'src/entities/meeting.entity';

@Controller('meetings')
@ApiUseTags('meetings')
export class MeetingsController {
    constructor(
        private meetingsService: MeetingsService,
    ) {
        //
    }

    @Get(':meetingId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get meeting', operationId: 'getMeeting' })
    @ApiResponse({ status: 200, description: 'Get meeting', type: GetMeetingResponse })
    @HttpCode(HttpStatus.OK)
    async get(@Param('meetingId') meetingId: string): Promise<GetMeetingResponse> {
        const findOneOptions: FindOneOptions<Meeting> = { where: { id: meetingId } };
        return await this.meetingsService.findOne(findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all meetings', operationId: 'getAllMeetings' })
    @ApiResponse({ status: 200, description: 'Get all meetings', type: GetMeetingsResponse })
    @HttpCode(HttpStatus.OK)
    async getAll(): Promise<GetMeetingsResponse> {
        return await this.meetingsService.findAll({});
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create', operationId: 'CreateMeeting' })
    @ApiResponse({ status: 200, description: 'Create', type: GetMeetingResponse })
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() meeting: MeetingDto): Promise<GetMeetingResponse> {
        return await this.meetingsService.create(meeting);
    }

    @Put()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update', operationId: 'UpdateMeeting' })
    @ApiResponse({ status: 200, description: 'Update', type: GetMeetingResponse })
    @HttpCode(HttpStatus.OK)
    async update(@Body() meeting: MeetingDto): Promise<GetMeetingResponse> {
        return await this.meetingsService.update(meeting);
    }

    @Delete(':meetingId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete', operationId: 'DeleteMeeting' })
    @ApiResponse({ status: 200, description: 'Delete', type: GenericResponse })
    @HttpCode(HttpStatus.OK)
    async delete(@Param('meetingId') meetingId: string): Promise<GenericResponse> {
        return await this.meetingsService.delete(meetingId);
    }
}
